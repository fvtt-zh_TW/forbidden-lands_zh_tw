Hooks.once('init', () => {

    if(typeof Babele !== 'undefined') {
        Babele.get().register({
            module: 'forbidden-lands_zh_tw',
            lang: 'zh-tw',
            dir: 'compendium'
        });
    }
});

